package ru.arubtsova.tm.api.controller;

public interface IProjectTaskController {

    void showAllTaskByProjectId();

    void removeProjectWithTasksById();

    void bindTaskToProject();

    void unbindTaskFromProject();

}
