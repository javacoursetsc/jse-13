package ru.arubtsova.tm.controller;

import ru.arubtsova.tm.api.controller.IProjectTaskController;
import ru.arubtsova.tm.api.service.IProjectTaskService;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.model.Task;
import ru.arubtsova.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showAllTaskByProjectId() {
        System.out.println("Tasks Overview:");
        System.out.println("Enter Project Id:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.findAllTaskByProjectId(projectId);
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public void removeProjectWithTasksById() {
        System.out.println("Project Removal:");
        System.out.println("Enter Project Id:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectTaskService.removeProjectWithTasksById(projectId);
        if (project == null) {
            System.out.println("Removal failed, please, try again");
        } else {
            System.out.println("Project was successfully removed");
        }
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("Enter Task Id:");
        final String taskId = TerminalUtil.nextLine();
        System.out.println("Enter Project Id:");
        final String projectId = TerminalUtil.nextLine();
        final Task task = projectTaskService.bindTaskToProject(taskId, projectId);
        if (task == null) {
            System.out.println("Can't bind Task to Project, please, try again");
        } else {
            System.out.println("Task was successfully bound to Project");
        }
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("Enter Task Id:");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskFromProject(taskId);
        if (task == null) {
            System.out.println("Can't unbind Task from Project, please, try again");
        } else {
            System.out.println("Task was successfully unbound from Project");
        }
    }

}
